package me.taucu.togglepvp.support;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import me.taucu.togglepvp.TogglePvp;
import me.taucu.togglepvp.TogglePvpAPI;

public class TogglePvpPAPIExpansion extends PlaceholderExpansion {

    final TogglePvpAPI api = TogglePvpAPI.get();
    final TogglePvp togglePvP = TogglePvp.get();

    @Override
    public String getIdentifier() {
        return "togglepvp";
    }

    @Override
    public String getVersion() {
        return "1.0.0";
    }

    @Override
    public String getAuthor() {
        return "TauCubed";
    }

    @Override
    public boolean canRegister() {
        return true;
    }

    @Override
    public String onRequest(OfflinePlayer oplayer, String identifier) {
        if (oplayer instanceof Player) {
            Player player = (Player) oplayer;
            switch (identifier.toLowerCase()) {
                case "state":
                    if (togglePvP.worldguardSupport != null && togglePvP.worldguardSupport.isPvpForceEnabled(player.getLocation())) {
                        return togglePvP.placeholderEnabledInRegion;
                    }
                    return api.getPvp(player) ? togglePvP.placeholderEnabled : togglePvP.placeholderDisabled;
                case "remainingtime":
                    return api.getFormattedRemainingTime(player);
                default:
                    break;
            }
        }
        return null;
    }
}
