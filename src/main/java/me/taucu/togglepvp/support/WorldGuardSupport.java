package me.taucu.togglepvp.support;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.flags.registry.FlagRegistry;
import me.taucu.togglepvp.TogglePvp;
import org.bukkit.Location;

public class WorldGuardSupport {

    public static final String FLAG_FORCE_PVP = "togglepvp-force-pvp";

    private final TogglePvp pl;
    private StateFlag forcePvpFlag;

    public WorldGuardSupport(TogglePvp pl) {
        this.pl = pl;
    }

    public void init() {
        FlagRegistry registry = WorldGuard.getInstance().getFlagRegistry();
        Flag<?> existing = registry.get(FLAG_FORCE_PVP);
        if (existing == null) {
            StateFlag flag = new StateFlag(FLAG_FORCE_PVP, false);

            registry.register(flag);
            forcePvpFlag = flag;
        } else {
            if (existing instanceof StateFlag) {
                forcePvpFlag = (StateFlag) existing;
            } else {
                pl.getLogger().severe("Could not create " + FLAG_FORCE_PVP + " flag. Flag of a different type is already registered.");
            }
        }
    }

    public boolean isPvpForceEnabled(Location loc) {
        return WorldGuard.getInstance().getPlatform().getRegionContainer()
                .createQuery().testState(BukkitAdapter.adapt(loc), null, forcePvpFlag);
    }

}
