package me.taucu.togglepvp.util;

import java.util.*;
import java.util.function.Function;

public class TimedEvictingLinkedMap<K, V> implements Map<K, V> {

    protected final LinkedHashMap<K, TimedValue<V>> map = new LinkedHashMap<>();
    protected Set<Entry<K, V>> entrySet = null;
    protected long timeout;
    protected long currentTick = 0;

    protected Function<V, K> keyResolver;

    public TimedEvictingLinkedMap(int timeout) {
        this(timeout, v -> {
            throw new UnsupportedOperationException("key resolver undefined");
        });
    }

    public TimedEvictingLinkedMap(int timeout, Function<V, K> keyResolver) {
        this.timeout = timeout;
        this.keyResolver = keyResolver;
    }

    @Override
    public V get(Object key) {
        TimedValue<V> val = map.get(key);
        return val == null ? null : val.value;
    }

    public V getOrDefault(Object k, V def) {
        V value = get(k);
        return value == null ? def : value;
    }

    public Map<K, TimedValue<V>> getBackingMap() {
        return map;
    }

    public V put(V v) throws UnsupportedOperationException {
        return put(keyResolver.apply(v), v);
    }

    @Override
    public V put(K k, V v) {
        TimedValue<V> val = map.put(k, new TimedValue<>(currentTick, v));
        return val == null ? null : val.value;
    }

    @Override
    public V remove(Object k) {
        TimedValue<V> val = map.remove(k);
        return val == null ? null : val.value;
    }

    public void tick() {
        currentTick++;
        removeTimeouts();
    }

    public boolean timedOut(TimedValue<V> entry) {
        return currentTick - entry.addedTick >= timeout;
    }

    public long getTimeout() {
        return timeout;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

    public boolean removeTimeouts() {
        boolean changed = false;
        if (timeout <= 1) {
            if (map.size() > 0 && timedOut(map.values().iterator().next())) map.clear();
        } else {
            Iterator<TimedValue<V>> it = map.values().iterator();
            while (it.hasNext() && timedOut(it.next())) {
                it.remove();
                changed = true;
            }
        }
        return changed;
    }

    public Function<V, K> getKeyResolver() {
        return keyResolver;
    }

    public void setKeyResolver(Function<V, K> keyResolver) {
        this.keyResolver = keyResolver;
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return map.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        for (TimedValue<V> tv : map.values()) {
            if (Objects.equals(tv.value, value)) return true;
        }
        return false;
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        for (Entry<? extends K, ? extends V> entry : m.entrySet()) {
            map.put(entry.getKey(), new TimedValue<>(currentTick, entry.getValue()));
        }
    }

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public Set<K> keySet() {
        return map.keySet();
    }

    @Override
    public Collection<V> values() {
        return new AbstractCollection<V>() {
            @Override
            public Iterator<V> iterator() {
                Iterator<TimedValue<V>> it = map.values().iterator();
                return new Iterator<V>() {
                    @Override
                    public boolean hasNext() {
                        return it.hasNext();
                    }

                    @Override
                    public V next() {
                        return it.next().value;
                    }

                    @Override
                    public void remove() {
                        it.remove();
                    }
                };
            }

            @Override
            public boolean contains(Object o) {
                return containsValue(o);
            }

            @Override
            public int size() {
                return TimedEvictingLinkedMap.this.size();
            }
        };
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        if (entrySet == null) {
            entrySet = new AbstractSet<Entry<K, V>>() {
                @Override
                public Iterator<Entry<K, V>> iterator() {
                    Iterator<Entry<K, TimedValue<V>>> it = map.entrySet().iterator();
                    return new Iterator<Entry<K, V>>() {
                        @Override
                        public boolean hasNext() {
                            return it.hasNext();
                        }

                        @Override
                        public Entry<K, V> next() {
                            Entry<K, TimedValue<V>> ent = it.next();
                            return new Entry<K, V>() {
                                @Override
                                public K getKey() {
                                    return ent.getKey();
                                }

                                @Override
                                public V getValue() {
                                    return ent.getValue().value;
                                }

                                @Override
                                public V setValue(V value) {
                                    V old = ent.getValue().value;
                                    ent.setValue(new TimedValue<>(currentTick, value));
                                    return old;
                                }
                            };
                        }

                        @Override
                        public void remove() {
                            it.remove();
                        }
                    };
                }

                @Override
                public int size() {
                    return TimedEvictingLinkedMap.this.size();
                }
            };
        }
        return entrySet;
    }

    public static class TimedValue<V> {
        public final long addedTick;
        public final V value;

        public TimedValue(long addedTick, V value) {
            this.addedTick = addedTick;
            this.value = value;
        }

    }

}
