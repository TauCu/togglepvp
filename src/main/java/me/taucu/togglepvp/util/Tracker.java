package me.taucu.togglepvp.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Supplier;

public class Tracker<V> {

    private final ArrayList<V> tracked = new ArrayList<>();
    private final Supplier<Long> tickSource;
    private long currentTick = -1;

    public Tracker(Supplier<Long> tickSource) {
        this.tickSource = tickSource;
    }

    public void add(V v) {
        resetIfTimedOut();
        add0(v);
    }

    public Collection<V> get() {
        resetIfTimedOut();
        return getTracked();
    }

    public Collection<V> getTracked() {
        return tracked;
    }

    protected boolean add0(V v) {
        return getTracked().add(v);
    }

    protected boolean remove0(V v) {
        return getTracked().remove(v);
    }

    public void tick() {
        resetIfTimedOut();
    }

    public long getTick() {
        return currentTick;
    }

    public void setTick() {
        setTick(tickSource.get());
    }

    public void setTick(long currentTick) {
        this.currentTick = currentTick;
    }

    public boolean timedOut() {
        return tickSource.get() - getTick() > 0;
    }

    public boolean resetIfTimedOut() {
        if (timedOut()) {
            getTracked().clear();
            setTick();
            return true;
        }
        return false;
    }

}
