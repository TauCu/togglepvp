package me.taucu.togglepvp.util;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.function.Predicate;

public class BlockUtil {

    public static BlockFace[] CARDINAL_FACES = new BlockFace[] {BlockFace.NORTH, BlockFace.SOUTH, BlockFace.WEST, BlockFace.EAST};
    public static BlockFace[] CARDINAL_Y_FACES = new BlockFace[] {BlockFace.UP, BlockFace.DOWN, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.WEST, BlockFace.EAST};

    public static final BlockFace[] ORDINAL_FACES = {BlockFace.NORTH_EAST, BlockFace.SOUTH_WEST, BlockFace.NORTH_WEST, BlockFace.SOUTH_EAST};
    public static final BlockFace[] ORDINAL_Y_FACES = {BlockFace.UP, BlockFace.DOWN, BlockFace.NORTH_EAST, BlockFace.SOUTH_WEST, BlockFace.NORTH_WEST, BlockFace.SOUTH_EAST};

    public static final BlockFace[] CARDINAL_ORDINAL_Y_FACES = {BlockFace.UP, BlockFace.DOWN, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.WEST, BlockFace.EAST, BlockFace.NORTH_EAST, BlockFace.SOUTH_WEST, BlockFace.NORTH_WEST, BlockFace.SOUTH_EAST};

    public static Set<Block> getConnectedBlocks(Block block, BlockFace[] faces, Material type, int max) {
        return getConnectedBlocks(block, faces, check -> check.getType() == type, max);
    }

    public static Set<Block> getConnectedBlocks(Block block, BlockFace[] faces, Predicate<Block> check, int max) {
        Set<Block> results = new HashSet<>();
        LinkedList<Block> pending = new LinkedList<>();

        pending.add(block);

        while ((block = pending.poll()) != null && results.size() < max) {
            for (BlockFace face : faces) {
                Block nearby = block.getRelative(face);
                if (check.test(nearby) && results.add(nearby)) {
                    pending.add(nearby);
                }
            }
        }
        return results;
    }

    public static Set<Block> getFuzzyConnectedBlocks(Block block, int fuzz, Material type, int max) {
        return getFuzzyConnectedBlocks(block, fuzz, fuzz, fuzz,  check -> check.getType() == type, max);
    }

    public static Set<Block> getFuzzyConnectedBlocks(Block block, int fuzz, Predicate<Block> check, int max) {
        return getFuzzyConnectedBlocks(block, fuzz, fuzz, fuzz, check, max);
    }

    public static Set<Block> getFuzzyConnectedBlocks(Block block, int xFuzz, int yFuzz, int zFuzz, Predicate<Block> check, int max) {
        Set<Block> results = new HashSet<>();
        LinkedList<Block> pending = new LinkedList<>();

        pending.add(block);

        while ((block = pending.poll()) != null && results.size() < max) {
            for (int modX = -xFuzz; modX <= xFuzz; modX++) {
                for (int modY = -yFuzz; modY <= yFuzz; modY++) {
                    for (int modZ = -zFuzz; modZ <= zFuzz; modZ++) {
                        Block nearby = block.getRelative(modX, modY, modZ);
                        if (check.test(nearby) && results.add(nearby)) {
                            pending.add(nearby);
                        }
                    }
                }
            }
        }
        return results;
    }

    public static Set<Block> getNearbyBlocks(Block block, int radius, Material type) {
        return getNearbyBlocks(block, radius, check -> check.getType() == type);
    }

    public static Set<Block> getNearbyBlocks(Block block, int xRadius, int yRadius, int zRadius, Material type) {
        return getNearbyBlocks(block, xRadius, yRadius, zRadius, check -> check.getType() == type);
    }

    public static Set<Block> getNearbyBlocks(Block block, int radius, Predicate<Block> check) {
        return getNearbyBlocks(block, radius, radius, radius, check);
    }

    public static Set<Block> getNearbyBlocks(Block block, int xRadius, int yRadius, int zRadius, Predicate<Block> check) {
        Set<Block> results = new HashSet<>();
        for (int modX = -xRadius; modX <= xRadius; modX++) {
            for (int modY = -yRadius; modY <= yRadius; modY++) {
                for (int modZ = -zRadius; modZ <= zRadius; modZ++) {
                    Block nearby = block.getRelative(modX, modY, modZ);
                    if (check.test(nearby)) {
                        results.add(nearby);
                    }
                }
            }
        }
        return results;
    }

}
