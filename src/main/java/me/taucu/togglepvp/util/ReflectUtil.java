package me.taucu.togglepvp.util;

import java.lang.reflect.Method;

public class ReflectUtil {

    public static Method getInheritedMethod(Class<?> where, String name) {
        return getInheritedMethod(where, name, new Class[0]);
    }

    public static Method getInheritedMethod(Class<?> where, String name, Class<?>... params) {
        do {
            try {
                return where.getDeclaredMethod(name, params);
            } catch (NoSuchMethodException ignored) {
                for (Class<?> clazz : where.getInterfaces()) {
                    Method m = getInheritedMethod(clazz, name, params);
                    if (m != null) {
                        return m;
                    }
                }
            }
        } while ((where = where.getSuperclass()) != null);
        return null;
    }

    public static Class<?> getClass(String name) {
        try {
            return Class.forName(name);
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    public static Class<?> getClass(String name, ClassLoader ldr) {
        try {
            return Class.forName(name, true, ldr);
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

}
