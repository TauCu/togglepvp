package me.taucu.togglepvp.util;

import org.bukkit.Location;

public class LocationUtil {

    public static Location toCenterLocation(Location loc) {
        return new Location(loc.getWorld(), loc.getBlockX() + 0.5, loc.getBlockY() + 0.5, loc.getBlockZ() + 0.5);
    }

}
