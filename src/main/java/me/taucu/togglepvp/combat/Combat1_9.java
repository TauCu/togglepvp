package me.taucu.togglepvp.combat;

import me.taucu.togglepvp.TogglePvp;
import org.bukkit.Bukkit;
import org.bukkit.entity.AreaEffectCloud;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.AreaEffectCloudApplyEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.LingeringPotionSplashEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.projectiles.ProjectileSource;

import java.util.Optional;
import java.util.stream.Stream;

public class Combat1_9 extends Combat {

    public Combat1_9(TogglePvp pl) {
        super(pl);
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onLingeringSplash(LingeringPotionSplashEvent e) {
        ProjectileSource s = e.getEntity().getShooter();
        if (s instanceof Player) {
            e.getAreaEffectCloud().setMetadata("Player", new FixedMetadataValue(pl, ((Player) s).getName()));
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onAreaCloudEffect(AreaEffectCloudApplyEvent e) {
        AreaEffectCloud cloud = e.getEntity();
        if (cloud.hasMetadata("Player")) {
            Optional<Player> dmgr = cloud.getMetadata("Player").stream()
                    .filter(m -> m.getOwningPlugin() == pl)
                    .map(MetadataValue::asString)
                    .map(Bukkit::getPlayerExact)
                    .findAny();
            if (dmgr.isPresent()) {
                Stream.concat(
                        Stream.of(cloud.getBasePotionData().getType().getEffectType()),
                        cloud.getCustomEffects().stream().map(PotionEffect::getType)
                ).filter(pl.offencivePots::contains).forEach(eff -> {
                    e.getAffectedEntities().removeIf(ent -> handle(dmgr.get(), ent, nonMeleeMessages, false, EntityDamageEvent.DamageCause.MAGIC));
                });
            } else {
                e.getAffectedEntities().clear();
            }
        }
    }

}
