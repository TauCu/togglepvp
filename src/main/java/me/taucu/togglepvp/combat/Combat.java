package me.taucu.togglepvp.combat;

import me.taucu.togglepvp.TogglePvp;
import me.taucu.togglepvp.events.DamageHandleEvent;
import me.taucu.togglepvp.util.BlockUtil;
import me.taucu.togglepvp.util.LocationUtil;
import me.taucu.togglepvp.util.ReflectUtil;
import me.taucu.togglepvp.util.TimedEvictingLinkedMap;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.util.Vector;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Combat implements Listener {

    protected final TogglePvp pl;

    private final boolean areaEffectCloudExists = ReflectUtil.getClass("org.bukkit.entity.AreaEffectCloud") != null;
    private final boolean sittableExists = ReflectUtil.getClass("org.bukkit.entity.Sittable") != null;
    private final Random rand = new Random();
    protected final TimedEvictingLinkedMap<Block, PlayerInteractEvent> interactTracker = new TimedEvictingLinkedMap<>(1, PlayerInteractEvent::getClickedBlock);
    protected final TimedEvictingLinkedMap<Entity, List<PlayerInteractEvent>> explosionTracker = new TimedEvictingLinkedMap<>(1);
    public HashMap<Material, Predicate<Block>> explodyBlocks = new HashMap<>();
    protected final Map<CommandSender, Long> msgCooldowns = new WeakHashMap<CommandSender, Long>();

    public String noEffect = "Couldn't Pull config MSG for 'noEffect'";
    public String hitCancelMsgBase = "Couldn't Pull config MSG for 'hitCancelMsgBase'";
    public String hitCancelMsgBoth = "Couldn't Pull config MSG for 'hitCancelMsgBoth'";
    public String hitCancelMsgYou = "Couldn't Pull config MSG for 'hitCancelMsgYou'";
    public String hitCancelMsgThem = "Couldn't Pull config MSG for 'hitCancelMsgThem'";
    public String targetOwnerOfflineMsg = "Couldn't Pull config MSG for 'TameOwnerOfflineMsg'";
    public boolean protectTame = false;
    public boolean protectFromUnknownExplosions;
    public boolean protectHazard = true;
    public boolean nonMeleeMessages;
    public boolean kb;
    public float kbForce = 0.5F;
    public int msgCooldown = 1000;

    public Combat(TogglePvp pl) {
        this.pl = pl;
        HashSet<String> dyeColors = Arrays.stream(DyeColor.values()).map(DyeColor::toString).collect(Collectors.toCollection(HashSet::new));
        explodyBlocks.clear();
        for (Material m : Material.values()) {
            String name = m.toString();
            switch (name) {
                case "RESPAWN_ANCHOR":
                    explodyBlocks.put(m, block -> block.getWorld().getEnvironment() != World.Environment.NETHER);
                    break;
                case "BED_BLOCK":
                    explodyBlocks.put(m, block -> block.getWorld().getEnvironment() != World.Environment.NORMAL);
                    break;
                default:
                    if (name.endsWith("_BED") && dyeColors.contains(name.substring(0, name.length()-4))) {
                        explodyBlocks.put(m, block -> block.getWorld().getEnvironment() != World.Environment.NORMAL);
                    }
                    break;
            }
        }
    }

    public void tick() {
        interactTracker.tick();
        explosionTracker.tick();
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onFlightToggle(PlayerToggleFlightEvent e) {
        enforceFlight(e.getPlayer(), e.isFlying());
    }

    public void enforceFlight(Player player) {
        enforceFlight(player, false);
    }

    public void enforceFlight(Player player, boolean flying) {
        if ((flying || player.getAllowFlight() || player.isFlying()) && pl.getState(player) && pl.getTime(player) + pl.delay >= System.currentTimeMillis() && !player.hasPermission("tau.togglepvp.flightbypass")) {
            player.setFlying(false);
            player.setAllowFlight(false);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onEntTeleport(EntityTeleportEvent e) {
        if (e.getEntity() instanceof Player && e.getFrom().getWorld() != e.getTo().getWorld() && pl.worldPvp.contains(e.getTo().getWorld().getName())) {
            sendMessage(e.getEntity(), noEffect);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onAiTarget(EntityTargetEvent e) {
        if (protectTame && e.getEntity() instanceof Tameable && e.getTarget() != null) {
            if (handle(e.getEntity(), e.getTarget(), false, false)) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
    public void onAnyDamage(EntityDamageEvent e) {
        DamageCause cause = e.getCause();
        Entity vic = e.getEntity();
        if (vic instanceof LivingEntity) {
            LivingEntity livingVic = (LivingEntity) vic;
            boolean offlineOwner = false;
            if (protectTame && livingVic instanceof Tameable) {
                Tameable tame = (Tameable) livingVic;
                if (tame.isTamed() && tame.getOwner() != null) {
                    Player p = Bukkit.getPlayer(tame.getOwner().getUniqueId());
                    if (p != null) {
                        livingVic = p;
                    } else {
                        offlineOwner = true;
                    }
                } else {
                    return;
                }
            }
            if (pl.isPvpForceEnabled(vic.getLocation())) {
                return;
            }
            switch (cause) {
                case BLOCK_EXPLOSION:
                    if (protectFromUnknownExplosions && (offlineOwner || !pl.getState(livingVic))) {
                        e.setCancelled(true);
                    }
                    break;
                case ENTITY_EXPLOSION:
                    if (protectFromUnknownExplosions && (offlineOwner || !(e instanceof EntityDamageByEntityEvent) && !pl.getState(livingVic))) {
                        e.setCancelled(true);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onEntDamageByEnt(EntityDamageByEntityEvent e) {
        Projectile proj = null;
        Entity dmgr = e.getDamager();
        boolean knockback = kb;
        boolean msg = true;
        if (dmgr instanceof ThrownPotion) {
            ThrownPotion po = (ThrownPotion) dmgr;
            if (po.getShooter() instanceof Player) {
                dmgr = (Entity) po.getShooter();
                msg = nonMeleeMessages;
                knockback = false;
            } else {
                return;
            }
        } else if (areaEffectCloudExists && dmgr instanceof AreaEffectCloud) {
            if (dmgr.hasMetadata("Player")) {
                List<MetadataValue> meta = dmgr.getMetadata("Player");
                Player player = null;
                for (MetadataValue m : meta) {
                    if (m.getOwningPlugin() == pl) {
                        player = Bukkit.getPlayerExact(m.asString());
                        break;
                    }
                }
                if (player != null) {
                    dmgr = player;
                    msg = nonMeleeMessages;
                    knockback = false;
                }
            }
        } else if (dmgr instanceof LightningStrike) {
            Entity vic = e.getEntity();
            if (protectTame && vic instanceof Tameable) {
                Tameable tame = (Tameable) vic;
                if (tame.isTamed() && tame.getOwner() instanceof OfflinePlayer) {
                    OfflinePlayer player = (OfflinePlayer) tame.getOwner();
                    if (player.isOnline()) {
                        vic = (Entity) tame.getOwner();
                    }
                } else {
                    return;
                }
            } else if (!(vic instanceof Player)) {
                return;
            }

            if (!pl.getState(vic) && !pl.isPvpForceEnabled(vic.getLocation())) {
                DamageHandleEvent dhe = DamageHandleEvent.call(vic, dmgr, null, e.getCause(), false, false);
                if (!dhe.isCancelled()) {
                    e.setCancelled(true);
                    BlockUtil.getNearbyBlocks(dhe.getVictim().getLocation().getBlock(), 1, 1, 1, Material.FIRE)
                            .forEach(b -> b.setType(Material.AIR));
                }
            }
            return;
        }

        // These things can cause each other, so we'll need to loop to get to the bottom of it
        if (dmgr instanceof TNTPrimed || dmgr instanceof EnderCrystal) {
            // safety in the event the cause is circular
            HashSet<Entity> damagers = new HashSet<>();
            while (dmgr instanceof TNTPrimed || dmgr instanceof EnderCrystal) {
                if (dmgr instanceof TNTPrimed) {
                    TNTPrimed tnt = (TNTPrimed) dmgr;
                    dmgr = tnt.getSource();
                    msg = nonMeleeMessages;
                    knockback = false;
                    if (dmgr == null) {
                        if (protectFromUnknownExplosions && !DamageHandleEvent.call(e.getEntity(), e.getDamager(), null, e.getCause(), false, false).isCancelled()) {
                            e.setCancelled(true);
                        }
                        return;
                    }
                } else if (dmgr instanceof EnderCrystal) {
                    EntityDamageEvent lastCause = dmgr.getLastDamageCause();
                    if (lastCause instanceof EntityDamageByEntityEvent) {
                        dmgr = ((EntityDamageByEntityEvent) lastCause).getDamager();
                        msg = nonMeleeMessages;
                        knockback = false;
                    } else {
                        dmgr = null;
                    }
                    if (dmgr == null) {
                        if (protectFromUnknownExplosions && !DamageHandleEvent.call(e.getEntity(), e.getDamager(), null, e.getCause(), false, false).isCancelled()) {
                            e.setCancelled(true);
                        }
                        return;
                    }
                }
                if (damagers.contains(dmgr)) {
                    break;
                } else {
                    damagers.add(dmgr);
                }
            }
        }

        // projectiles can cause explosions so calculate last
        if (dmgr instanceof Projectile) {
            proj = (Projectile) dmgr;
            if (proj.getShooter() instanceof Entity) {
                dmgr = (Entity) proj.getShooter();
                msg = nonMeleeMessages;
            }
        }

        if (handle(dmgr, e.getEntity(), msg, knockback, e.getCause())) {
            e.setCancelled(true);
            if (proj instanceof Arrow) {
                proj.remove();
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onCombust(EntityCombustByEntityEvent e) {
        Entity pt = e.getCombuster();
        if (pt instanceof Projectile) {
            Projectile p = (Projectile) pt;
            if (p.getShooter() instanceof Entity) {
                if (handle((Entity) p.getShooter(), e.getEntity(), false, false, DamageCause.PROJECTILE)) {
                    e.setCancelled(true);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPotionSplash(PotionSplashEvent e) {
        ProjectileSource ps = e.getPotion().getShooter();
        if (ps instanceof Player) {
            Player dmgr = (Player) ps;
            for (PotionEffect eff : e.getPotion().getEffects()) {
                if (pl.offencivePots.contains(eff.getType())) {
                    for (LivingEntity ent : e.getAffectedEntities()) {
                        if (handle(dmgr, ent, nonMeleeMessages, false, DamageCause.MAGIC)) {
                            e.setIntensity(ent, 0);
                        }
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onIgnite(BlockIgniteEvent e) {
        if (protectHazard && e.getPlayer() != null) {
            if (runHazardLogic(e.getBlock(), e.getPlayer(), 2)) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onBucketEmpty(PlayerBucketEmptyEvent e) {
        if (protectHazard && e.getBucket() == Material.LAVA_BUCKET) {
            if (runHazardLogic(e.getBlockClicked().getRelative(e.getBlockFace()), e.getPlayer(), e.getBlockClicked().getWorld().getEnvironment() == World.Environment.NETHER ? 8 : 4)) {
                e.setCancelled(true);
            }
        }
    }

    public boolean runHazardLogic(Block hazard, Entity dmgr, double distance) {
        for (Entity nearby : hazard.getWorld().getNearbyEntities(LocationUtil.toCenterLocation(hazard.getLocation()), distance, distance, distance)) {
            if (nearby != dmgr) {
                if (nearby instanceof Player) {
                    Player nearbyPlayer = (Player) nearby;
                    GameMode nearMode = nearbyPlayer.getGameMode();
                    if (nearMode == GameMode.SPECTATOR || nearMode == GameMode.CREATIVE || nearbyPlayer.isInvulnerable()) continue;
                    if (dmgr instanceof Player && !nearbyPlayer.canSee((Player) dmgr)) continue;
                }
                return handle(dmgr, nearby, true, false);
            }
        }
        return false;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onInteract(PlayerInteractEvent e) {
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            Predicate<Block> check = explodyBlocks.get(e.getClickedBlock().getType());
            if (check != null && check.test(e.getClickedBlock())) {
                Location loc = e.getClickedBlock().getLocation();
                for (Entity ent : loc.getWorld().getNearbyEntities(loc, 16, 16, 16)) {
                    if (ent instanceof Player || ent instanceof Tameable) {
                        List<PlayerInteractEvent> events = explosionTracker.get(ent);
                        if (events == null) {
                            events = new ArrayList<>();
                            explosionTracker.put(ent, events);
                        }
                        events.add(e);
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onEntityDamageByBlock(EntityDamageByBlockEvent e) {
        Collection<PlayerInteractEvent> causes = explosionTracker.get(e.getEntity());
        if (causes != null) {
            for (PlayerInteractEvent ie : causes) {
                if (handle(ie.getPlayer(), e.getEntity(), true, false, DamageCause.BLOCK_EXPLOSION)) {
                    e.setCancelled(true);
                }
            }
        }
    }

    public boolean handle(Entity dmgr, Entity vic, boolean msg, boolean knockback) {
        return handle0(dmgr, dmgr, vic, msg, knockback, null);
    }

    public boolean handle(Entity dmgr, Entity vic, boolean msg, boolean knockback, DamageCause cause) {
        return handle0(dmgr, dmgr, vic, msg, knockback, cause);
    }

    private boolean handle0(Entity originalDmgr, Entity dmgr, Entity vic, boolean msg, boolean knockback, DamageCause cause) {
        if (vic.hasMetadata("NPC") || dmgr.hasMetadata("NPC")) {
            return false;
        }

        if (dmgr instanceof Tameable) {
            Tameable tame = (Tameable) dmgr;
            if (tame.isTamed() && tame.getOwner() != null) {
                Player owner = Bukkit.getPlayer(tame.getOwner().getUniqueId());
                if (owner == null) {
                    if (pl.isPvpForceEnabled(dmgr.getLocation(), vic.getLocation()) || DamageHandleEvent.call(vic, originalDmgr, null, cause, msg, knockback).isCancelled()) {
                        return false;
                    } else {
                        if (sittableExists && tame instanceof Sittable) {
                            ((Sittable) tame).setSitting(true);
                        }
                        if (tame instanceof Creature) {
                            // required for 1.13 and lower
                            //noinspection RedundantCast
                            ((Creature) tame).setTarget(null);
                        }
                        if (tame instanceof Wolf) {
                            ((Wolf) tame).setAngry(false);
                        }
                        return true;
                    }
                } else {
                    if (owner.hasMetadata("NPC")) {
                        return false;
                    } else {
                        if (!pl.isPvpForceEnabled(dmgr.getLocation(), vic.getLocation()) && handle(owner, vic, false, false, cause)) {
                            if (tame instanceof Creature) {
                                // required for 1.13 and lower
                                //noinspection RedundantCast
                                ((Creature) tame).setTarget(null);
                            }
                            if (tame instanceof Wolf) {
                                ((Wolf) tame).setAngry(false);
                            }
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            }
        }

        Entity pet = null;
        if (protectTame && vic instanceof Tameable) {
            Tameable tame = (Tameable) vic;
            if (tame.isTamed() && tame.getOwner() != null) {
                Player owner = Bukkit.getPlayer(tame.getOwner().getUniqueId());
                if (owner == null) {
                    DamageHandleEvent dhe = DamageHandleEvent.call(vic, originalDmgr, dmgr, cause, msg, knockback);
                    if (dhe.isCancelled() || pl.isPvpForceEnabled(dmgr.getLocation(), vic.getLocation())) {
                        return false;
                    } else {
                        if (dhe.isSendMsg() && msgCooldown(dhe.getResolvedDamager())) {
                            sendMessage(dhe.getResolvedDamager(), targetOwnerOfflineMsg);
                        }
                        return true;
                    }
                } else {
                    if (owner.hasMetadata("NPC")) {
                        return false;
                    } else {
                        pet = vic;
                        vic = owner;
                    }
                }
            }
        }

        DamageHandleEvent dhe = DamageHandleEvent.call(vic, originalDmgr, dmgr, cause, msg, knockback);
        if (dhe.isCancelled()) {
            return false;
        } else {
            dmgr = dhe.getResolvedDamager();
            vic = dhe.getVictim();
            msg = dhe.isSendMsg();
            knockback = dhe.isKnockback();
        }

        if (dmgr instanceof Player && vic instanceof Player) {
            if (pl.isPvpForceEnabled(dmgr.getLocation(), vic.getLocation()) || (pet != null && pl.isPvpForceEnabled(pet.getLocation())) || dmgr == vic) {
                return false;
            }

            if (!pl.getState(vic) || !pl.getState(dmgr)) {
                if (kb && knockback) {
                    knockback((Player) dmgr, pet == null ? vic : pet);
                }
                if (msg && msgCooldown(dmgr)) {
                    sendMessage(dmgr, hitCancelMsgBase.replace("%player%", vic.getName()));
                    if (!pl.getState(dmgr) && !pl.getState(vic)) {
                        sendMessage(dmgr, hitCancelMsgBoth.replace("%player%", vic.getName()));
                    } else {
                        if (!pl.getState(dmgr)) {
                            sendMessage(dmgr, hitCancelMsgYou);
                        } else {
                            sendMessage(dmgr, hitCancelMsgThem.replace("%player%", vic.getName()));
                        }
                    }
                }
                return true;
            }
            pl.setTime((Player) vic, System.currentTimeMillis());
            pl.setTime((Player) dmgr, System.currentTimeMillis());
            enforceFlight((Player) vic);
            enforceFlight((Player) dmgr);
        }
        return false;
    }

    public void knockback(Player dmgr, Entity vic) {
        dmgr.setVelocity(normalizeUnlessZeroRandom(vic.getLocation().toVector().subtract(dmgr.getLocation().toVector())).multiply(-kbForce));
    }

    public Vector normalizeUnlessZeroRandom(Vector vec) {
        if (vec.length() > 0) {
            return vec.normalize();
        } else {
            return vec
                    .setX(rand.nextBoolean() ? rand.nextDouble() : -rand.nextDouble())
                    .setY(rand.nextBoolean() ? rand.nextDouble() : -rand.nextDouble())
                    .setZ(rand.nextBoolean() ? rand.nextDouble() : -rand.nextDouble());
        }
    }

    public void sendMessage(CommandSender s, String msg) {
        if (msg != null && !msg.isEmpty()) {
            s.sendMessage(msg);
        }
    }

    public boolean msgCooldown(CommandSender s) {
        Long t = msgCooldowns.get(s);
        if (t == null || t < System.currentTimeMillis() - msgCooldown) {
            msgCooldowns.put(s, System.currentTimeMillis());
            return true;
        }
        return false;
    }

}