package me.taucu.togglepvp.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class PlayerLoadedEvent extends PlayerEvent {

    private static final HandlerList HANDLERS = new HandlerList();

    public PlayerLoadedEvent(Player who) {
        super(who);
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }

    /**
     * Creates and calls a PlayerLoadedEvent
     * @param who the player to be used in the event
     */
    public static void call(Player who) {
        PlayerLoadedEvent event = new PlayerLoadedEvent(who);
        Bukkit.getPluginManager().callEvent(event);
    }

}
