package me.taucu.togglepvp.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class PlayerPvpChangedEvent extends PlayerEvent implements Cancellable {

    private static final HandlerList HANDLERS = new HandlerList();

    private final boolean newState;

    private boolean cancelled = false;

    public PlayerPvpChangedEvent(Player who, boolean newState) {
        super(who);
        this.newState = newState;
    }

    public boolean getNewState() {
        return newState;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }

    /**
     * creates and calls a PvpChangedEvent
     *
     * @param who      the player
     * @param newState the advertised new pvp state
     * @return true if not cancelled false otherwise
     */
    public static boolean call(Player who, boolean newState) {
        PlayerPvpChangedEvent event = new PlayerPvpChangedEvent(who, newState);
        Bukkit.getPluginManager().callEvent(event);
        return !event.cancelled;
    }

}
