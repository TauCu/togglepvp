package me.taucu.togglepvp.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityEvent;

public class DamageHandleEvent extends EntityEvent implements Cancellable {

    private static final HandlerList HANDLERS = new HandlerList();

    private Entity victim;
    private Entity damager;
    private Entity resolvedDamager;
    private DamageCause cause;
    private boolean sendMsg;
    private boolean knockback;

    private boolean cancelled;

    public DamageHandleEvent(Entity victim, Entity damager, Entity resolvedDamager, DamageCause cause, boolean sendMsg, boolean knockback) {
        super(victim);
        this.victim = victim;
        this.damager = damager;
        this.resolvedDamager = resolvedDamager;
        this.cause = cause;
        this.sendMsg = sendMsg;
        this.knockback = knockback;
    }

    public Entity getVictim() {
        return victim;
    }

    public void setVictim(Entity victim) {
        this.victim = victim;
    }

    /**
     * gets the damager of the victim
     *
     * @return the damager (may be null if non-entity damage)
     */
    public Entity getDamager() {
        return damager;
    }

    public void setDamager(Entity damager) {
        this.damager = damager;
    }

    /**
     * gets the resolved damager of the victim
     *
     * @return the resolved damaging entity (normally player) (null if no entity could be resolved for this damage)
     */
    public Entity getResolvedDamager() {
        return resolvedDamager;
    }

    public void setResolvedDamager(Entity resolvedDamager) {
        if (resolvedDamager == null && this.resolvedDamager != null) {
            resolvedDamager = damager;
        }
        this.resolvedDamager = resolvedDamager;
    }

    public DamageCause getCause() {
        return cause;
    }

    public void setCause(DamageCause cause) {
        this.cause = cause;
    }

    public boolean isSendMsg() {
        return sendMsg;
    }

    public void setSendMsg(boolean sendMsg) {
        this.sendMsg = sendMsg;
    }

    public boolean isKnockback() {
        return knockback;
    }

    public void setKnockback(boolean knockback) {
        this.knockback = knockback;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }

    /**
     * creates and calls a DamageHandleEvent
     *
     * @param victim  the victim
     * @param damager the nullable damager
     * @param resolvedDamager the nullable resolved damager
     * @param cause the cause of the damage or null if no damage was dealt (normally in the event of EntityTargetEvent or when a Hazard is detected)
     * @return the new DamageHandleEvent
     */
    public static DamageHandleEvent call(Entity victim, Entity damager, Entity resolvedDamager, DamageCause cause, boolean sendMsg, boolean knockback) {
        DamageHandleEvent event = new DamageHandleEvent(victim, damager, resolvedDamager, cause, sendMsg, knockback);
        Bukkit.getPluginManager().callEvent(event);
        return event;
    }

}
