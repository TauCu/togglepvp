package me.taucu.togglepvp;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

public class TogglePvpAPI {
    
    private static TogglePvpAPI instance = null;
    private static TogglePvp pl;
    
    public static TogglePvpAPI get() {
        if (instance == null) {
            instance = new TogglePvpAPI(TogglePvp.get());
        }
        return instance;
    }
    
    protected TogglePvpAPI(TogglePvp togglePvP) {
        instance = this;
        pl = togglePvP;
    }
    
    /*
     * 
     * Section for getting information.
     * 
     */
    
    private DecimalFormat time_df = new DecimalFormat("0.00");
    
    /**
     * @param player the player
     * @return true if a player has pvp, false if not (Or Offline)
     */
    public boolean getPvp(Player player) {
        return pl.getState(player);
    }
    
    /**
     * @return all players that have PVP enabled.
     */
    public Collection<Player> getEnabledPlayers() {
        return Collections.unmodifiableCollection(pl.toggle.keySet());
    }
    
    /**
     * @return Returns an unmodifiable map of players who have pvp enabled, the time is above 0 if they have been in combat at one stage
     */
    public Map<Player, Long> getToggleTimeMap() {
        return Collections.unmodifiableMap(pl.toggle);
    }
    
    /**
     * @param player the player
     * @return the amount of time (Milliseconds) remaining before a player is allowed to toggle their pvp off.
     */
    public long getRemainingToggleTimeMillis(Player player) {
        long time = System.currentTimeMillis();
        if (pl.getState(player) && pl.getTime(player) + pl.delay >= time) {
            return pl.getTime(player) - time;
        }
        return 0;
    }
    
    /**
     * @param player the player
     * @return the formatted remaining time before a player is allowed to toggle their pvp off.
     */
    public String getFormattedRemainingTime(Player player) {
        Long remainingTime = getRemainingToggleTimeMillis(player) / 1000;
        return time_df.format(remainingTime);
    }
    
    /**
     * @param player the player
     * @return true if a player has been out of combat long enough to toggle their pvp off.
     */
    public boolean timeCanToggle(Player player) {
        return getRemainingToggleTimeMillis(player) == 0;
    }
    
    /**
     * @return the amount of delay in the config that a player must be out of combat for before it will allow them to turn their pvp off.
     */
    public int getDelayMillis() {
        return pl.delay;
    }
    
    /*
     * 
     * Section for setting information.
     * 
     */
    
    /**
     * Set a user's pvp state.
     * @param state the new pvp state for that player
     * @param player the player
     */
    public void setPvp(boolean state, Player player) {
        setPvp(state, player, false);
    }

    /**
     * Set a user's pvp state.
     * @param state the new pvp state for that player
     * @param player the player
     * @param callEvent should we call a PlayerPvpChangedEvent and respect its outcome
     * @return true if the pvp state was set, false if the player is not online or the event was cancelled
     */
    public boolean setPvp(boolean state, Player player, boolean callEvent) {
        if (player.isOnline()) {
            return pl.setState(player, state, true, callEvent);
        }
        return false;
    }
    
    /**
     * Sets remaining toggle time for a user, Will not do so if the user does not have PVP enabled.
     * @param time the new time delta for that player
     * @param player the player
     */
    public void setToggleTimeMillis(int time, Player player) {
        if (player.isOnline() && getPvp(player) && time >= 0) {
            pl.setTime(player, System.currentTimeMillis() + time);
        }
    }

    /**
     * Checks if pvp is force enabled at this location
     * @param where the location to check
     * @return true if it is, false otherwise
     */
    public boolean isPvpForceEnabled(Location where) {
        return pl.isPvpForceEnabled(where);
    }
    
}
