package me.taucu.togglepvp;

import me.taucu.togglepvp.combat.Combat;
import me.taucu.togglepvp.combat.Combat1_9;
import me.taucu.togglepvp.events.PlayerLoadedEvent;
import me.taucu.togglepvp.events.PlayerPvpChangedEvent;
import me.taucu.togglepvp.support.TogglePvpPAPIExpansion;
import me.taucu.togglepvp.support.WorldGuardSupport;
import me.taucu.togglepvp.util.ReflectUtil;
import org.bstats.bukkit.Metrics;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.NamespacedKey;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.text.DecimalFormat;
import java.util.*;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.stream.Collectors;

public class TogglePvp extends JavaPlugin implements Listener {

    public NamespacedKey playerMapKey = null;
    private boolean dataContSupported = false;

    static TogglePvp plugin;
    public Combat combat = null;
    private int tickTask = -1;
    public long currentTick = -1;
    public boolean defaultState = false;
    public String toggleperm = "tau.togglepvp.use";
    public String reloadperm = "tau.togglepvp.reload";
    public int delay = 10000;
    public List<String> usageMsg = new ArrayList<String>();
    public List<String> miscMsg = new ArrayList<String>();
    public List<String> timeoutMsg = new ArrayList<String>();
    public List<String> worlds = new ArrayList<String>();
    public List<String> pvpEnableCmds = new ArrayList<String>();
    public HashSet<String> combatBlockedCmds = new HashSet<String>();
    public String combatBlockedCmdMsg = ChatColor.RED + "This command is disabled during combat";
    public List<String> bannedPotionEffects;
    public ArrayList<String> usageMsgs = new ArrayList<String>();
    public String pvpAlreadyDisabled = "Couldn't Pull config MSG for 'pvpAlreadyDisabled'";
    public String pvpAlreadyEnabled = "Couldn't Pull config MSG for 'pvpAlreadyEnabled'";
    public String pvpDisabled = "Couldn't Pull config MSG for 'pvpDisabled'";
    public String pvpEnabled = "Couldn't Pull config MSG for 'pvpEnabled'";
    public String noEffect = "Couldn't Pull config MSG for 'noEffect'";
    protected boolean pluginEnable = false;

    public Map<Player, Long> toggle = new HashMap<Player, Long>();
    public HashSet<PotionEffectType> offencivePots = new HashSet<PotionEffectType>();
    public HashSet<String> worldPvp = new HashSet<String>();

    public String tabPveFormat = ChatColor.GREEN + "";
    public String tabPvpFormat = ChatColor.RED + "";

    public boolean stateEffects = false;
    public Team glowPvpTeam = null;
    public Team glowPveTeam = null;

    public String placeholderEnabled;
    public String placeholderDisabled;
    public String placeholderEnabledInRegion;

    public WorldGuardSupport worldguardSupport;

    public static TogglePvp get() {
        return plugin;
    }

    @Override
    public void onLoad() {
        if (Bukkit.getPluginManager().getPlugin("WorldGuard") != null) {
            if (ReflectUtil.getClass("com.sk89q.worldguard.WorldGuard") == null) {
                getLogger().severe("You are using an incompatible version of WorldGuard");
            } else {
                try {
                    WorldGuardSupport worldguardSupport = new WorldGuardSupport(this);
                    worldguardSupport.init();
                    this.worldguardSupport = worldguardSupport;
                } catch (Throwable t) {
                    if (t instanceof ThreadDeath) {
                        throw t;
                    }
                    getLogger().log(Level.SEVERE, "Error while registering flags with WorldGuard", t);
                }
            }
        }
    }

    @Override
    public void onEnable() {
        plugin = this;
        combat = resolveCombat();
        
        tickTask = Bukkit.getScheduler().scheduleSyncRepeatingTask(this, () -> {
            currentTick++;
            combat.tick();
        }, 0, 1);

        this.saveDefaultConfig();
        this.reloadConfig();

        if (getConfig().getInt("Config Version") != getConfig().getDefaults().getInt("Config Version")) {
            getLogger().warning("Your config is outdated.");
            getLogger().warning("You should update your config by deleting it and letting the plugin regenerate it.");
            getLogger().warning("(Make sure to copy your settings as needed)");
        }

        defaultState = this.getConfig().getBoolean("Default Pvp State");

        delay = this.getConfig().getInt("Delay") * 1000;
        toggleperm = this.getConfig().getString("Toggle Permission");
        reloadperm = this.getConfig().getString("Reload Permission");

        pvpEnableCmds = this.getConfig().getStringList("Pvp Enabled Commands");

        combatBlockedCmds.addAll(this.getConfig().getStringList("Blocked Combat Commands"));
        combatBlockedCmdMsg = ChatColor.translateAlternateColorCodes('&', this.getConfig().getString("Blocked Combat Command Message"));

        worldPvp.clear();
        worldPvp.addAll(this.getConfig().getStringList("Force PVP Enabled Worlds"));

        usageMsg = this.getConfig().getStringList("MSG Usage").stream().map(s -> ChatColor.translateAlternateColorCodes('&', s)).collect(Collectors.toList());
        noEffect = ChatColor.translateAlternateColorCodes('&', this.getConfig().getString("MSG In PVP Enabled World ON Pvp CMD"));
        combat.noEffect = noEffect;
        timeoutMsg = this.getConfig().getStringList("MSG CombatTagged").stream().map(s -> ChatColor.translateAlternateColorCodes('&', s)).collect(Collectors.toList());
        pvpAlreadyDisabled = ChatColor.translateAlternateColorCodes('&', this.getConfig().getString("MSG Pvp is Already DISABLED"));
        pvpAlreadyEnabled = ChatColor.translateAlternateColorCodes('&', this.getConfig().getString("MSG Pvp is Already ENABLED"));
        pvpDisabled = ChatColor.translateAlternateColorCodes('&', this.getConfig().getString("MSG Pvp DISABLED"));
        pvpEnabled = ChatColor.translateAlternateColorCodes('&', this.getConfig().getString("MSG Pvp ENABLED"));

        combat.hitCancelMsgBase = ChatColor.translateAlternateColorCodes('&', this.getConfig().getString("MSG Attack Blocked BASE"));
        combat.hitCancelMsgBoth = ChatColor.translateAlternateColorCodes('&', this.getConfig().getString("MSG Attack Blocked Both Players Disabled"));
        combat.hitCancelMsgYou = ChatColor.translateAlternateColorCodes('&', this.getConfig().getString("MSG Attack Blocked You Disabled"));
        combat.hitCancelMsgThem = ChatColor.translateAlternateColorCodes('&', this.getConfig().getString("MSG Attack Blocked Other Player Disabled"));
        combat.targetOwnerOfflineMsg = ChatColor.translateAlternateColorCodes('&', this.getConfig().getString("Animal Owner Offline"));
        combat.nonMeleeMessages = this.getConfig().getBoolean("Send PVP Disabled Messages On Non-Melee");
        combat.kb = this.getConfig().getBoolean("Enable Knockback");
        combat.kbForce = Float.parseFloat(this.getConfig().getString("Knockback Force"));
        combat.msgCooldown = (int) (this.getConfig().getDouble("Attack MSG Cooldown") * 1000);

        offencivePots.clear();
        if (this.getConfig().getStringList("Banned Potion Effects") != null) {
            for (String ps : this.getConfig().getStringList("Banned Potion Effects")) {
                PotionEffectType type = PotionEffectType.getByName(ps);
                if (type == null) {
                    getLogger().warning("Unknown banned potion effect: " + ps);
                } else {
                    offencivePots.add(type);
                }
            }
        }

        miscMsg.clear();
        miscMsg.add(ChatColor.translateAlternateColorCodes('&', this.getConfig().getConfigurationSection("Misc").getString("No Permission")));
        miscMsg.add(ChatColor.translateAlternateColorCodes('&', this.getConfig().getConfigurationSection("Misc").getString("ConsoleIsNotPlayer")));
        miscMsg.add(ChatColor.translateAlternateColorCodes('&', this.getConfig().getConfigurationSection("Misc").getString("Reload")));
        miscMsg.add(ChatColor.translateAlternateColorCodes('&', this.getConfig().getConfigurationSection("Misc").getString("Reloading")));
        miscMsg.add(ChatColor.translateAlternateColorCodes('&', this.getConfig().getConfigurationSection("Misc").getString("ConsoleCannotPvp")));

        combat.protectTame = this.getConfig().getBoolean("Protect Tameable Animals");
        combat.protectFromUnknownExplosions = this.getConfig().getBoolean("Protect From Unknown Explosions");
        combat.protectHazard = this.getConfig().getBoolean("Protect From Player Hazards");

        {
            ConfigurationSection stateSec = this.getConfig().getConfigurationSection("State Effects");
            stateEffects = stateSec.getBoolean("enabled");

            Scoreboard board = Bukkit.getScoreboardManager().getMainScoreboard();
            glowPvpTeam = board.getTeam("me.taucu.togglepvp.pvp_team");
            if (glowPvpTeam != null) {
                glowPvpTeam.unregister();
                glowPvpTeam = null;
            }

            glowPveTeam = board.getTeam("me.taucu.togglepvp.pve_team");
            if (glowPveTeam != null) {
                glowPveTeam.unregister();
                glowPveTeam = null;
            }

            if (stateEffects) {
                tabPveFormat = ChatColor.translateAlternateColorCodes('&', stateSec.getString("pve tab name"));
                tabPveFormat = tabPveFormat.isEmpty() ? null : tabPveFormat;
                tabPvpFormat = ChatColor.translateAlternateColorCodes('&', stateSec.getString("pvp tab name"));
                tabPvpFormat = tabPvpFormat.isEmpty() ? null : tabPvpFormat;

                // required for 1.11 and lower
                if (ReflectUtil.getInheritedMethod(Team.class, "setColor", ChatColor.class) != null) {
                    String onColor = stateSec.getString("pvp glow color");
                    if (onColor.length() > 0) {
                        org.bukkit.ChatColor color = org.bukkit.ChatColor.valueOf(onColor.toUpperCase());
                        glowPvpTeam = board.registerNewTeam("me.taucu.togglepvp.pvp_team");
                        glowPvpTeam.setColor(color);
                    }

                    String offColor = stateSec.getString("pve glow color");
                    if (offColor.length() > 0) {
                        org.bukkit.ChatColor color = org.bukkit.ChatColor.valueOf(offColor.toUpperCase());
                        glowPveTeam = board.registerNewTeam("me.taucu.togglepvp.pve_team");
                        glowPveTeam.setColor(color);
                    }
                } else {
                    getLogger().warning("this server is outdated and doesn't support glowing teams");
                }
            }
        }

        placeholderEnabled = ChatColor.translateAlternateColorCodes('&', getConfig().getString("placeholders.pvp enabled"));
        placeholderDisabled = ChatColor.translateAlternateColorCodes('&', getConfig().getString("placeholders.pvp disabled"));
        placeholderEnabledInRegion = ChatColor.translateAlternateColorCodes('&', getConfig().getString("placeholders.pvp region"));

        if (this.getConfig().getBoolean("Enable Persistent Pvp State")) {
            // required for 1.13 and lower
            if (ReflectUtil.getInheritedMethod(Player.class, "getPersistentDataContainer") != null) {
                dataContSupported = true;
                playerMapKey = new NamespacedKey(this, "togglePvpTime");
            } else {
                dataContSupported = false;
                getLogger().warning("This server is outdated and doesn't support the persistence of pvp states");
            }
        }

        for (Player p : Bukkit.getOnlinePlayers()) {
            load(p);
        }

        new TogglePvpAPI(this);

        // papi
        if (Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI")) {
            getLogger().info("Registering PlaceholderAPI Expansions");
            new TogglePvpPAPIExpansion().register();
        }

        if (worldguardSupport != null) {
            getLogger().info("WorldGuard support loaded.");
        }

        Bukkit.getPluginManager().registerEvents(combat, this);
        Bukkit.getPluginManager().registerEvents(this, this);

        new Metrics(this, 16647);

        getLogger().info("Enabled TogglePvp");
        pluginEnable = true;
    }

    private Combat resolveCombat() {
        String mcVer = Bukkit.getBukkitVersion().split("-")[0];
        int major = 0, minor = 0;
        try {
            String[] vers = mcVer.replaceAll("[^.\\d]", "").split("\\.");
            major = Integer.parseInt(vers[0]);
            minor = Integer.parseInt(vers[1]);
        } catch (ArrayIndexOutOfBoundsException ignored) {
            getLogger().log(Level.SEVERE, "Could not format server version. Defaulting to lowest support for: " + mcVer);
        } catch (NumberFormatException e) {
            getLogger().log(Level.SEVERE, "Could not format server version. Defaulting to lowest support for: " + mcVer, e);
        }

        if (major > 0 && minor > 8) {
            return new Combat1_9(this);
        } else {
            return new Combat(this);
        }
    }


    @Override
    public void onDisable() {
        if (glowPvpTeam != null) {
            glowPvpTeam.unregister();
            glowPvpTeam = null;
        }
        if (glowPveTeam != null) {
            glowPveTeam.unregister();
            glowPveTeam = null;
        }

        worldPvp.clear();
        offencivePots.clear();
        for (Player p : toggle.keySet()) {
            save(p);
        }
        toggle.clear();
        miscMsg.clear();
        usageMsg.clear();
        pvpEnableCmds.clear();
        tickTask = -1;
        worldguardSupport = null;
        pluginEnable = false;
        HandlerList.unregisterAll((Plugin) this);
        Bukkit.getScheduler().cancelTasks(this);
        getLogger().info("Disabled TogglePvp");
    }

    public boolean setState(Player who, boolean state) {
        return setState(who, state, 0, true, true);
    }

    public boolean setState(Player who, boolean state, boolean overwrite) {
        return setState(who, state, 0, overwrite, true);
    }

    public boolean setState(Player who, boolean state, boolean overwrite, boolean callEvent) {
        return setState(who, state, 0, overwrite, callEvent);
    }

    public boolean setState(Player who, boolean state, long time) {
        return setState(who, state, time, true, true);
    }

    public boolean setState(Player who, boolean state, long time, boolean overwrite) {
        return setState(who, state, time, overwrite, true);
    }

    public boolean setState(Player who, boolean state, long time, boolean overwrite, boolean callEvent) {
        if (state) {
            if (overwrite || !toggle.containsKey(who)) {
                if (callEvent && !PlayerPvpChangedEvent.call(who, true)) {
                    return false;
                }

                toggle.put(who, time);

                if (stateEffects) {
                    if (tabPvpFormat != null) {
                        who.setPlayerListName(tabPvpFormat.replace("%name%", who.getName()));
                    } else if (tabPveFormat != null) {
                        who.setPlayerListName(who.getName());
                    }

                    if (glowPveTeam != null) {
                        who.setGlowing(false);
                        glowPveTeam.removeEntry(who.getName());
                    }
                    if (glowPvpTeam != null && !glowPvpTeam.hasEntry(who.getName())) {
                        glowPvpTeam.addEntry(who.getName());
                        who.setGlowing(true);
                    }
                }

                runPvpEnabledCommands(who);
            }
            combat.enforceFlight(who);
        } else {
            if (callEvent && !PlayerPvpChangedEvent.call(who, false)) {
                return false;
            }

            toggle.remove(who);

            if (stateEffects) {
                if (tabPveFormat != null) {
                    who.setPlayerListName(tabPveFormat.replace("%name%", who.getName()));
                } else if (tabPvpFormat != null) {
                    who.setPlayerListName(who.getName());
                }

                if (glowPvpTeam != null) {
                    who.setGlowing(false);
                    glowPvpTeam.removeEntry(who.getName());
                }
                if (glowPveTeam != null && !glowPveTeam.hasEntry(who.getName())) {
                    glowPveTeam.addEntry(who.getName());
                    who.setGlowing(true);
                }
            }
        }
        return true;
    }

    public boolean getState(Entity who) {
        return toggle.containsKey(who);
    }

    public void setTime(Player who, long time) {
        if (toggle.containsKey(who)) {
            toggle.put(who, time);
        }
    }

    public Long getTime(Entity who) {
        return toggle.get(who);
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        load(e.getPlayer());
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        if (getState(e.getPlayer())) {
            save(e.getPlayer());
            setState(e.getPlayer(), false);
        }
    }

    public void load(Player p) {
        if (dataContSupported) {
            PersistentDataContainer cont = p.getPersistentDataContainer();
            if (cont.has(playerMapKey, PersistentDataType.LONG)) {
                Long lastTime = p.getPersistentDataContainer().get(playerMapKey, PersistentDataType.LONG);
                if (lastTime >= 0) {
                    setState(p, true, lastTime, true, false);
                } else {
                    setState(p, false, true, false);
                }
            } else {
                setState(p, defaultState, true, false);
                save(p);
            }
        } else if (defaultState) {
            setState(p, true, true, false);
        }

        combat.enforceFlight(p);

        PlayerLoadedEvent.call(p);
    }

    public void save(Player p) {
        if (dataContSupported) {
            if (getState(p)) {
                p.getPersistentDataContainer().set(playerMapKey, PersistentDataType.LONG, getTime(p));
            } else {
                p.getPersistentDataContainer().set(playerMapKey, PersistentDataType.LONG, Long.MIN_VALUE);
            }
        }
    }

    @EventHandler
    public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent e) {
        Long time = getTime(e.getPlayer());
        if (time != null && combatBlockedCmds.contains(e.getMessage().toLowerCase()) && time + delay > System.currentTimeMillis()) {
            e.setCancelled(true);
            e.getPlayer().sendMessage(combatBlockedCmdMsg);
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("togglepvp")) {
            if (sender.hasPermission(toggleperm)) {
                if (!(sender instanceof Player)) {
                    sender.sendMessage(miscMsg.get(4));
                    return true;
                }

                Player p = (Player) sender;
                if (worldPvp.contains(p.getWorld().getName())) {
                    sender.sendMessage(noEffect);
                }

                long time = System.currentTimeMillis();
                if (getState(p) && getTime(p) + delay >= time) {
                    time = time - getTime(p);
                    double remainingtime = delay - time;
                    remainingtime = remainingtime / 1000;
                    DecimalFormat df = new DecimalFormat("0.00");
                    for (String m : timeoutMsg) {
                        String msg = m.replace("%remainingtime%", "" + df.format(remainingtime));
                        sender.sendMessage(msg);
                    }
                    return true;
                } else {

                    if (args.length == 1) {
                        if (args[0].equalsIgnoreCase("off") || args[0].equalsIgnoreCase("disable") || args[0].equalsIgnoreCase("stop")) {
                            if (getState(p)) {
                                if (setState(p, false)) {
                                    save(p);
                                    sender.sendMessage(pvpDisabled);
                                }
                            } else {
                                sender.sendMessage(pvpAlreadyDisabled);
                            }
                            return true;
                        }
                    }

                    if (args.length == 1) {
                        if (args[0].equalsIgnoreCase("on") || args[0].equalsIgnoreCase("enable") || args[0].equalsIgnoreCase("start")) {
                            if (!getState(p)) {
                                if (setState(p, true)) {
                                    save(p);
                                    sender.sendMessage(pvpEnabled);
                                }
                            } else {
                                sender.sendMessage(pvpAlreadyEnabled);
                            }
                            return true;
                        }
                    }

                    boolean toggle = false;
                    if (args.length == 1) {
                        if (args[0].equalsIgnoreCase("toggle") || args[0].equalsIgnoreCase("pvp")) {
                            toggle = true;
                        }
                    }

                    if (toggle || args.length == 0) {
                        if (getState(p)) {
                            if (setState(p, false)) {
                                sender.sendMessage(pvpDisabled);
                                save(p);
                            }
                            return true;
                        }
                    }

                    if (toggle || args.length == 0) {
                        if (!getState(p)) {
                            if (setState(p, true)) {
                                sender.sendMessage(pvpEnabled);
                                save(p);
                            }
                            return true;
                        }
                    }
                }
                for (String i : usageMsg) {
                    sender.sendMessage(i);
                }
            } else {
                sender.sendMessage(miscMsg.get(0));
            }
            return true;
        }

        if (cmd.getName().equalsIgnoreCase("togglepvpReload")) {
            if (sender.hasPermission(reloadperm)) {
                sender.sendMessage(miscMsg.get(2));
                onDisable();
                onEnable();
                sender.sendMessage(miscMsg.get(3));
            } else {
                sender.sendMessage(miscMsg.get(0));
                return true;
            }
        }

        if (cmd.getName().equalsIgnoreCase("togglepvpGetWorld")) {
            if (sender.isOp() || sender.hasPermission(reloadperm)) {

                if (!(sender instanceof Player)) {
                    sender.sendMessage(miscMsg.get(1));
                    return true;
                }

                Player p = (Player) sender;
                sender.sendMessage(p.getWorld().getName());
            } else {
                sender.sendMessage(miscMsg.get(0));
                return true;
            }
        }

        return false;
    }

    public void runPvpEnabledCommands(CommandSender sender) {
        for (String s : pvpEnableCmds) {
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), s.replace("%player%", sender.getName()));
        }
    }

    public static long currentTick() {
        return plugin.currentTick;
    }

    public boolean isPvpForceEnabled(Location a, Location b) {
        return isPvpForceEnabled(a) && isPvpForceEnabled(b);
    }
    
    public boolean isPvpForceEnabled(Location where) {
        if (worldPvp.contains(where.getWorld().getName())) {
            return true;
        } else if (worldguardSupport != null) {
            return worldguardSupport.isPvpForceEnabled(where);
        }
        return false;
    }

    public void repeatTask(int times, int delay, Function<Integer, Boolean> action) {
        repeatTask(times, delay, delay, action);
    }

    public void repeatTask(int times, int interval, int delay, Function<Integer, Boolean> action) {
        int start = 1;
        if (delay < 1 && !action.apply(start++)) {
            return;
        }
        final int fstart = start;
        int[] atomic = new int[] {0};
        Runnable ticker = new Runnable() {
            int cstart = fstart;
            @Override
            public void run() {
                if (!action.apply(cstart++) || cstart > times) {
                    Bukkit.getScheduler().cancelTask(atomic[0]);
                }
            }
        };
        atomic[0] = Bukkit.getScheduler().scheduleSyncRepeatingTask(this, ticker, delay, interval);
    }

}
